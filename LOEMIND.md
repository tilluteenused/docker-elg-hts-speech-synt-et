# Eesti keele HTS-kõnesünteesi konteiner

[Eesti keele HTS-kõnesünteesi](https://github.com/ikiissel/synthts_et) sisaldav tarkvara konteiner (docker), mille liides vastab [ELG nõuetele](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Mida sisaldab <a name="Mida_sisaldab"></a>

* [Eesti keele HTS-kõnesüntees](https://github.com/ikiissel/synthts_et), millesse on integreeritud [Filosofti eesti keele morfoloogiline analüsaator](https://github.com/Filosoft/vabamorf/LOEMIND.md)
* Konteineri ja liidesega seotud lähtekood

## Eeltingimused

* Peab olema paigaldatud tarkvara konteineri tegemiseks/kasutamiseks; juhised on [docker'i veebilehel](https://docs.docker.com/).
* Kui sooviks on lähtekoodi ise kompileerida või konteinerit kokku panna, siis peab olema paigaldatud versioonihaldustarkvara; juhised on [git'i veebilehel](https://git-scm.com/).


## Konteineri allalaadimine Docker Hub'ist

Valmis konteineri saab laadida alla Docker Hub'ist, kasutades Linux'i käsurida (Windows'i/Mac'i käsurida on analoogiline):

```commandline
docker pull tilluteenused/elg_eki_synthts_et:1.0.0
```

 Seejärel saab jätkata osaga [Konteineri käivitamine](#Konteineri_käivitamine).

## Ise konteineri tegemine

### 1. Lähtekoodi allalaadimine

<!---
Lähtekood koosneb 2 osast
1. ELG json, veebiserver ja konteieri tegemise asjad
2. Indreku kõnesünteesi asjad mis sisaldavad ka mingit vabamorfi morf analüsaatori versiooni.
IK on selle oma reposse ümberkopeerinud.
---->

```commandline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker-elg-hts-speech-synt-et.git gitlab_docker_elg_hts_speech_synt_et/
```
Repositoorium sisaldab eelkompileeritud koodi ja andmefaile:

* **_synthts_et_** on kompileeritud kõnesünteesi programm.
* **_et.dct_**  ja **_et3.dct_** on kõnesünteesi programmi poolt kasutava
[Filosofti morfoloogilise analüsaatori ja ühestaja andmefailid](https://github.com/Filosoft/vabamorf/LOEMIND.md) .
* **_.htsvoice_** laiendiga failid sisaldavad erinevaid sünteeskõne hääli.

Seejärel saab jätkata osaga [Konteineri kokkupanemine](#Konteineri_kokkupanemine).

### 1.1. Lisavõimalus: HTS-kõnesüntesaatori allalaadimine ja kompileerimine <a name="#EKI_TTS"></a>


Eelkompileeritud koodi kasutamise asemel saab soovi korral   sünteeskõneks vajaliku tarkvara ka ise kokku kompileerida, kasutes alljärgnevat [Eesti keele HTS-kõnesünteesi](https://github.com/ikiissel/synthts_et.git) lehelt pärit juhendit.

* Allalaadimine:

```commandline
cd ~
git clone https://github.com/ikiissel/synthts_et.git github-synthts_et/
```
* Kompileerimine:

```commandline
cd ~/github-synthts_et/synthts_et/
test -x configure || autoreconf -vif
./configure CC=/usr/bin/gcc CXX=/usr/bin/g++ CFLAGS="-g3 -gdwarf-2" CXXFLAGS="-g3 -gdwarf-2"
make
```

* Kompileeritud kõnesünteesi programmi ja andmefailide kopeerimine konteineri tegemise tarkvara kataloogi:

```commandline
cp ~/github-synthts_et/synthts_et/bin/synthts_et ~/github-synthts_et/synthts_et/dct/* ~/github-synthts_et/synthts_et/htsvoices/* ~/gitlab-docker-elg/gitlab_docker_elg_hts_speech_synt_et
```

### 2. Konteineri kokkupanemine <a name="Konteineri_kokkupanemine"></a>

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_elg_hts_speech_synt_et/
docker build -t tilluteenused/elg_eki_synthts_et:1.0.0 .
```

## Konteineri käivitamine <a name="Konteineri_käivitamine"></a>

```commandline
docker run -p 7000:7000 tilluteenused/elg_eki_synthts_et:1.0.0
```

Pole oluline, milline on jooksev kataloog terminaliaknas konteineri käivitamise hetkel.

Käivitatud konteineri töö lõpetab Ctrl+C selles terminaliaknas, kust konteiner käivitati. 

## Päringu json-kuju

```json
{
  "params":{"cmdline": []}   /* LIPUD, synthts_et käsu parameetrid, iga parameeter eraldi stringis */
  "type":"text",
  "content": string, /* Sünteesitav tekst */
}
```

JSON struktuuri elementidele kommentaarina lisatud sõnad näitavad vastavust sünteesiprogrammi sisendiga, nagu neid käsitleb kõnesüntesaator: params = LIPUD, content = sünteesitav tekst.

Lippude kaudu saab ette anda sünteeshäält (**_-m_**) ja esituskiirust (**_-r_**). Lähemalt vaata 
[EKI HTS-kõnesüntesaatori GitHub'i lehelt](https://github.com/ikiissel/synthts_et.git) ja [näiteid](#kasutusnäited)

## Vastuse kuju

Väljundiks on **_.wav_** formaadis helifail.

## Kasutusnäited <a name="kasutusnäited"></a>

```cmdline
curl --request POST --header "Content-Type: application/json" --data '{"type": "text", "content": "Mingi natukene pikem jutt on see siin."}' -D header.txt -o content.wav http://localhost:7000/process ; grep -q 'HTTP/.* 200 OK' header.txt && aplay content.wav || cat header.txt
```

```cmdline
curl --request POST --header "Content-Type: application/json" --data '{"params": {"cmdline":["-m", "eki_et_eva.htsvoice", "-r", "1.0"]}, "type": "text", "content":  "tere Talv"}' -D header.txt -o content.wav http://localhost:7000/process ; grep -q 'HTTP/.* 200 OK' header.txt && aplay content.wav || cat header.txt
```

Sünteesitud kõne salvestatakse antud juhul faili **_content.wav_** ja
veebiserveri poolt tagastatud päisefail faili **_header.txt_**.


Märkused:

* **_-D_** parameetriga antud päisefaili nime **_curl_**'i päringus 
võite valida oma parima äranägemise järgi. 
Päisefailis sisalduv info võimaldab kontrollida, kas päring oli edukas. 
Päringu õnnestumise korral sisaldab päisefaili esimene rida stringi **_200 OK_**, ebaõnnestumise korral **_BAD REQUEST_** vms.

* **_-o_** parameetriga antud helifaili nime **_curl_**'i päringus 
võib valida oma parima äranägemise järgi (välja arvatud failinime laiend, 
see peab olema **_.wav_**).
Seda, kas helifaili genereerimine õnnestus, saab kontrollida päisefailis oleva info abil.

## Rahastus

Konteiner loodi EL projekti [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry) toel.

## Autorid

Konteineri autorid: Tarmo Vaino, Heiki-Jaan Kaalep

Konteineri sisu autoreid vt. jaotises [Mida sisaldab](#Mida_sisaldab) toodud viidetest.
 
