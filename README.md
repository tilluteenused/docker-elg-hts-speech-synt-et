# Container with HTS speech synthesis for Estonian

[HTS speech synthesis for Estonian](https://github.com/ikiissel/synthts_et) container (docker) with
interface compliant with [ELG requirements](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Contains  <a name="Contains"></a>

* [HTS speech synthesis for Estonian](https://github.com/ikiissel/synthts_et), including an integrated [Filosoft morphological analyzer](https://github.com/Filosoft/vabamorf)
* Container and interface code

## Preliminaries

* You should have software for making / using the container installed; see instructions on the [docker web site](https://docs.docker.com/).
* In case you want to compile the code or build the container yourself, you should have version control software installed; see instructions on the [git web site](https://git-scm.com/).

## Downloading image from Docker Hub

You may dowload a ready-made container from Docker Hub, using the Linux command line (Windows / Mac command lines are similar):

```commandline
docker pull tilluteenused/elg_eki_synthts_et:1.0.0
```

Next, continue to the section [Starting the container](#Starting_the_container).

## Making your own container

### 1. Downloading the source code

<!---
Lähtekood koosneb 2 osast
1. ELG json, veebiserver ja konteieri tegemise asjad
2. Indreku kõnesünteesi asjad mis sisaldavad ka mingit vabamorfi morf analüsaatori versiooni.
IK on selle oma reposse ümberkopeerinud.
---->

```commandline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker-elg-hts-speech-synt-et.git gitlab_docker_elg_hts_speech_synt_et/
```
The repo contains a compiled pre-compiled code and data files:

* **_synthts_et_** is a compiled programme for speech synthesis.
* **_et.dct_**  and **_et3.dct_** are lexicons of the
[Filosoft morphological analyzer](https://github.com/Filosoft/vabamorf), used by the speech synthesis programme.
* **_.htsvoice_** is the extension for voice files.


Next, continue to the section [Building the container](#Building_the_container).

### 1.1. Additional possibility: downloading and compiling the HTS speech synthesizer <a name="#EKI_TTS"></a>

Instead of using the pre-compiled code, you may compile the code yourself, following the instructions from the [HTS speech synthesis for Estonian](https://github.com/ikiissel/synthts_et.git) web page.

* Downloading:

```commandline
cd ~
git clone https://github.com/ikiissel/synthts_et.git github-synthts_et/
```
* Compiling:

```commandline
cd ~/github-synthts_et/synthts_et/
test -x configure || autoreconf -vif
./configure CC=/usr/bin/gcc CXX=/usr/bin/g++ CFLAGS="-g3 -gdwarf-2" CXXFLAGS="-g3 -gdwarf-2"
make
```

* Copying the compiled code and data files to the directory of the container building software:

```commandline
cp ~/github-synthts_et/synthts_et/bin/synthts_et ~/github-synthts_et/synthts_et/dct/* ~/github-synthts_et/synthts_et/htsvoices/* ~/gitlab-docker-elg/gitlab_docker_elg_hts_speech_synt_et
```

### 2. Building the container <a name="Building_the_container"></a>

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_elg_hts_speech_synt_et/
docker build -t tilluteenused/elg_eki_synthts_et:1.0.0 .
```

## Starting the container <a name="Starting_the_container"></a>

```commandline
docker run -p 7000:7000 tilluteenused/elg_eki_synthts_et:1.0.0
```

One need not be in a specific directory to start the container.

Ctrl+C in a terminal window with a running container in it will terminate the container.

## Query json

```json
{
  "params":{"cmdline": []}   /* LIPUD, synthts_et käsu parameetrid, iga parameeter eraldi stringis */
  "type":"text",
  "content": string, /* Sünteesitav tekst */
}
```

The comments added to the JSON structure elements indicate the corresponding input and parameters of the speech synthesis programme: params = FLAGS, content = text that will be spoken.


The flags may be used for tuning the synthetic voice (**_-m_**)  and speech rate (**_-r_**). See the [GitHub page of the HTS speech synthesis for Estonian](https://github.com/ikiissel/synthts_et.git) and [examples](#examples).

## Response

The response is a sound file in **_.wav_** format.

## Examples <a name="examples"></a>

```cmdline
curl --request POST --header "Content-Type: application/json" --data '{"type": "text", "content": "Mingi natukene pikem jutt on see siin."}' -D header.txt -o content.wav http://localhost:7000/process ; grep -q 'HTTP/.* 200 OK' header.txt && aplay content.wav || cat header.txt
```

```cmdline
curl --request POST --header "Content-Type: application/json" --data '{"params": {"cmdline":["-m", "eki_et_eva.htsvoice", "-r", "1.0"]}, "type": "text", "content":  "tere Talv"}' -D header.txt -o content.wav http://localhost:7000/process ; grep -q 'HTTP/.* 200 OK' header.txt && aplay content.wav || cat header.txt
```

The speech is saved as **_content.wav_** and the responce header is saved as **_header.txt_**.

Notes:

* You are free to choose the header file name when using the **_-D_** parameter in **_curl_**. The header file enables one to check whether the query was successful: its first line should contain the string **_200 OK_**; in case of failure it contains **_BAD REQUEST_** or something else.
* You are free to choose the sound file name when using the **_-o_** parameter in **_curl_**, but its extension must be **_.wav_**.  

## Sponsors

The container development was sponsored by EU CEF project [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry)


## Authors

Authors of the container: Tarmo Vaino, Heiki-Jaan Kaalep

Authors of the contents of the container: see references at section [Contains](#Contains).
 
