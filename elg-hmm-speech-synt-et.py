#!/usr/bin/env python3

"""
# command line script:
# build virtual environment:
./create_venv.sh
# example in .vscode/launch.json

# web server in docker & curl
docker build -t  tilluteenused/elg_eki_synthts_et:1.0.0 .
docker run -p 7000:7000  tilluteenused/elg_eki_synthts_et:1.0.0

# docker push tilluteenused/elg_eki_synthts_et:1.0.0
# docker pull tilluteenused/elg_eki_synthts_et:1.0.0

curl --request POST --header "Content-Type: application/json" --data '{"type": "text", "content": "Mingi natukene pikem jutt on see siin."}' -D 2022-header.txt -o 2022-content.wav http://localhost:7000/process ; grep -q 'HTTP/.* 200 OK' 2022-header.txt && aplay 2022-content.wav || cat 2022-header.txt
curl --request POST --header "Content-Type: application/json" --data '{"params": {"cmdline":["-m", "eki_et_eva.htsvoice", "-r", "1.0"]}, "type": "text", "content":  "tere Talv"}' -D 2022-header.txt -o 2022-content.wav http://localhost:7000/process ; grep -q 'HTTP/.* 200 OK' 2022-header.txt && aplay 2022-content.wav || cat 2022-header.txt
"""

import os
from datetime import datetime
import subprocess
from typing import Dict
from flask import Flask, Response, request
from flask import abort
from flask_json import FlaskJSON

#from flask_json import json_response, as_json
#from flask import send_file

app = Flask(__name__)
# Don't add an extra "status" property to JSON responses - this would break the API contract
app.config['JSON_ADD_STATUS'] = False
# Don't sort properties alphabetically in response JSON
app.config['JSON_SORT_KEYS'] = False

json_app = FlaskJSON(app)


def run_synthts_et(data: Dict, fn_txt: str, fn_wav: str) -> None:
    """ Sünteesitud heli faili filename.wav

    Args:
        data (Dict):
        * data["content"]  tekst sünteesimiseks
        * data["params"]["cmdline"] sünteesiprogrammi parameetrite muutmuiseks

        fn_txt (str): Ajutine abifail sünteesitava teksti hoidmiseks
        fn_wav (str): Sünteeskõne faili fn_wav
    """
    my_params = []  # run synthts_et with default flags
    if ("params" in data) and ("cmdline" in data["params"]):
        my_params = data["params"]["cmdline"]  # add given flags

    content = data['content']
    # sünteesime sisendtekstist helifaili
    with open(fn_txt, "w+", encoding="utf8") as textfile:
        textfile.write(content)
    #  korjame välja parameetrid ja paneme käsu kokku
    cmd = ['./synthts_et', '-lex', './et.dct', '-lexd', 'et3.dct',
                    '-m', './eki_et_tnu.htsvoice', '-r', '1.1',
                    '-f',  fn_txt, '-o', fn_wav] \
                    + my_params

    #result = subprocess.run(cmd, stdout=subprocess.PIPE, check=True)
    # result.stdout.decode('utf-8')
    subprocess.run(cmd, stdout=subprocess.PIPE, check=True)

    # sünteesitu tagasi
    # return send_file(f'{filename}.wav',  mimetype="audio/wav",
    #    as_attachment=True, attachment_filename="content.wav")
    if os.path.exists(fn_txt):
        os.remove(fn_txt)

@app.route('/process', methods=['POST'])
def process_request():
    """Main request processing logic - accepts a JSON request and returns a wav file."""
    try:
        data = request.get_json()
    except Exception:
        abort(400)

    # sanity checks on the request message
    if (data.get('type') != 'text') or ('content' not in data):
        abort(400)

    filename = datetime.utcnow().strftime('%Y-%m-%d_%H-%M-%S_000%f')
    run_synthts_et(data, f'{filename}.txt', f'{filename}.wav')
    data = ''
    with os.path.exists(f'{filename}.wav') and open(f'{filename}.wav', "rb") as fwav:
        data = fwav.read()
        os.remove(f'{filename}.wav')
    return Response(data, mimetype="audio/x-wav")

def run_server() -> None:
    ''' Run as flask webserver '''
    app.run()

def run_cmdline(data_str: str):
    ''' Just for debbuging, no other point to use it that way '''
    data = json.loads(data_str)
    if "params" in data and "out.wav" in data["params"]:
        filename = datetime.utcnow().strftime('%Y-%m-%d_%H-%M-%S_000%f')
        run_synthts_et(data, f'{filename}.txt', data["params"]["out.wav"])
    else:
        print(f'{sys.argv[0]}:Bad JSON')
        sys.exit(1)

if __name__ == '__main__':
    import argparse
    import json
    import sys
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-j', '--json', type=str, help='sentences to precrocess')
    args = argparser.parse_args()
    if args.json is None:
        run_server()
    else:
        run_cmdline(args.json)
